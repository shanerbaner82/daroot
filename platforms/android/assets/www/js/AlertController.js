angular.module( 'starter').controller('AlertController', function ($scope,  $http, $ionicBackdrop, $state) {


            $scope.sendMessage = function(message){
                $ionicBackdrop.retain();
                var m = message.message;
                var state = message.state;
                var license = message.license;
                $http.get("http://104.236.200.201/push/"+m+"/"+state+"/"+license).
                    success(function(d,s){
                        $ionicBackdrop.release();
                        alert("Driver Alerted");
                    }).
                    error(function(d,s){
                        $ionicBackdrop.release();
                    });
            };

        $scope.alertMessage = function(accident){
            $ionicBackdrop.retain();
            var state = accident.state;
            var license = accident.license;
            $http.get("http://104.236.200.201/sms/"+license+"/"+state+"/was just involved in an accident").
            success(function(d,s){
                    $ionicBackdrop.release();
                    alert('Contacts Notified, Call 911!');
                }).
                error(function(d,s){
                    $ionicBackdrop.release();
                    alert('That Driver Does Not Have An Account, Call 911!');
                });
        };
    });

