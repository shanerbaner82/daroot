angular.module( 'app.register', [
])
    .config(function($stateProvider) {
        $stateProvider.state('main.register', {
            url: '/register',
            views: {
                'main-register': {
                    templateUrl: 'templates/main-register.html',
                    controller: 'RegistrationController'
                }
            }
        });
    })
    .controller('RegistrationController', function($scope,  $http, $state, $cordovaPush, $rootScope, $ionicBackdrop, $ionicNavBarDelegate) {
        $ionicNavBarDelegate.title('Register');
        $scope.register = function(newUser){
            $ionicBackdrop.retain();
            getGCMID();
            newUser.gcmid = window.localStorage.getItem("regid");
            $http.post('http://104.236.200.201/auth/register', JSON.stringify(newUser)).
                success(function(data, status, headers, config) {
                    window.localStorage.setItem("email", newUser.email);
                    $ionicBackdrop.release();
                    $state.go("tab.cars");
                }).
                error(function(data, status, headers, config) {
                    $ionicBackdrop.release();
                    alert("error " + status);
                });
        };

        var getGCMID = function(){
            var androidConfig = {
                "senderID": "794718568948"
            };
            $cordovaPush.register(androidConfig);
            $rootScope.$on('$cordovaPush:notificationReceived', function (event, notification) {
                switch (notification.event) {
                    case 'registered':
                        if (notification.regid.length > 0) {
                            window.localStorage.setItem('regid', notification.regid);
                        }
                        break;

                    case 'message':
                        // this is the actual push notification. its format depends on the data model from the push server
                        alert( notification.message );
                        break;

                    case 'error':
                        alert('GCM error = ' + notification.msg);
                        break;

                    default:
                        alert('An unknown GCM event has occurred');
                        break;
                }
            });
        }

    });



